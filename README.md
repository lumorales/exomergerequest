# Exercice

Le but de cet exercice est d'apprendre les manipulations à effectuer pour travailler à plusieurs sur un même document.

## Organisation
* Les étudiants se regroupent par 4 dans l'ordre de présence dans la salle dans le sens horaire.
* Une personne est nommée responsable de groupe, cette personne est chargée de merger le travail des autres et de demander un merge request sur le projet principal.

## Manipulations
* Le responsable de groupe fork le projet de référence sur son compte.
* Les étudiants du groupe fork le projet du responsable d'équipe sur leur compte.
* Chaque étudiant clone le fork du projet de son compte sur son poste.
* Chaque étudiant ajoute le projet du responsable de groupe comme remote.
* Chaque étudiant ajoute le projet principal comme remote.
* Chaque étudiant modifie le fichier de son groupe pour ajouter son nom et prénom dans la case du tableau qui lui correspond.
* Chaque étudiant  fait une demande de merge request sur le projet du responsable de groupe.
* Le responsable de groupe merge le travaille du groupe et fait une demande de merge à l'enseignant.
* L'enseignant merge le travail des différents groupes.
* Les étudiants récupèrent le travail de leur groupe et des autres groupes pour mettre à jour leur poste et leur fork.
* À la fin de l'exercice, tout le monde doit avoir sur son poste la version avec tous les noms.
